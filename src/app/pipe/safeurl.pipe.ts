import {DomSanitizer} from '@angular/platform-browser';
import {PipeTransform, Pipe} from "@angular/core";

@Pipe({ name: 'safeUrl' })

export class SafeUrlPipe implements PipeTransform {
	saafUrl;
	constructor(private sanitizer: DomSanitizer) {}
	transform(url) {

		if(url != undefined && url != null){
			url = url.replace("watch?v=", "embed/");
			this.saafUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
			return this.saafUrl;
		}else{
			return url;
		}
	}
}
