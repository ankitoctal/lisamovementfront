import { NgModule }   from '@angular/core';
import { SafeUrlPipe }  from './safeurl.pipe';

 @NgModule({
     imports:        [],
     declarations:   [SafeUrlPipe],
     exports:        [SafeUrlPipe],
 })

 export class PipeModule {

   static forRoot() {
      return {
          ngModule: PipeModule,
          providers: [],
      };
   }
 } 