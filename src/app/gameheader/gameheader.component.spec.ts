import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameheaderComponent } from './gameheader.component';

describe('GameheaderComponent', () => {
  let component: GameheaderComponent;
  let fixture: ComponentFixture<GameheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
