import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from '../__services/common.service';
import { UserService } from '../__services/user.service';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-gameheader',
  templateUrl: './gameheader.component.html',
  styleUrls: ['./gameheader.component.css']
})
export class GameheaderComponent implements OnInit {
  public contentSettings : any = {};
  public urlSegmentCheck :any;
  public parentRoute:any=[];
  constructor(
  	private commonService: CommonService,
  	private activatedRoute: ActivatedRoute,
  	public userService:UserService,
    private toastr: ToastrService,
  	public router: Router) {
    	this.urlSegmentCheck = activatedRoute.snapshot.url;
   }

 	ngOnInit(): void {
      	this.activatedRoute.parent.url.subscribe((urlPath) => {
	        if(urlPath[0].path!==""){  
	          this.parentRoute = urlPath[urlPath.length - 1].path;     
	        }
    	})

	    this.commonService.gameName('main-page').subscribe((response: any) => {
	        this.contentSettings = response.data; 
	    });
  	}

}
