import { Component, OnInit } from '@angular/core';
import { FormGroup , Validators, FormBuilder, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { UserService } from '../__services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: []
})

export class InviteComponent implements OnInit {

	sendInvitesForm: FormGroup;
 	submitted = false;
 	currentUser:any = {};
	resposne = [];
	errorMsg:any={};

 	constructor(
	 	private formBuilder: FormBuilder,
	    private userService: UserService,
	    private router: Router,
	    private toastr:ToastrService
	) { 
		var user = localStorage.getItem('currentUser');
		this.currentUser = JSON.parse(user);
		console.log(this.currentUser);
	}

	ngOnInit(): void {
		this.sendInvitesForm = this.formBuilder.group({
			team_id:[''],
			user_role_id:[this.currentUser.role_id],
			email:['', [Validators.required,Validators.email]]
		});
	}


	// convenience getter for easy access to form fields
	get f() { return this.sendInvitesForm.controls; }

	sendInviteLink(){
			this.submitted = true;

		    // stop here if form is invalid
		    if (this.sendInvitesForm.invalid) {
		      return;
		    }

		    this.userService.sendInviteLink(this.sendInvitesForm.value).subscribe(
		      	(data:any) => {
			    	if(data['status'] == 1) {
			            this.toastr.success(data['message']);
			        } else if(data['status'] == 0) {
			            this.toastr.error(data['message']);
			        } else {
			            this.toastr.error(data['message']);
			        }
			    },
			    error =>{
			        this.toastr.error(error.message);
			    }
		    )
	}

}
