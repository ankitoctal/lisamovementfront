import { Component, OnInit } from '@angular/core';
import { CommonService } from '../__services/common.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';

@Component({
    selector:'app-footer',
    templateUrl:'./footer.component.html',
    styleUrls:[]
})

export class FooterComponent implements OnInit {

	stage:any = 5;
	sponser:any;
	env = environment;

    constructor(
    	private commonService: CommonService,
	    private router: Router,
	    private toastr:ToastrService
    ) { }

    ngOnInit() {
    	this.commonService.getSponserLogo(this.stage).subscribe(
			(response:any) => {
					if(response['status'] == 1) {
						this.sponser = response.data;
					}
				},
				error =>{
					this.toastr.error(error.message);
				}
			)

    }
}
