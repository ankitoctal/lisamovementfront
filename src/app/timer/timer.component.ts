import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownComponent } from 'ngx-countdown';
@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  status = 'start';
  @ViewChild('countdown') counter: CountdownComponent;

  finishTest() {
    console.log("count down", this.counter);
    this.status = 'finished';
    this.counter.restart();
  }

  resetTimer(){
    console.log("count down", this.counter);
      this.counter.restart();
  }
  startTimer(){
    console.log("count down", this.counter);
      this.counter.resume();
  }
  pauseTimer(){
    console.log("count down", this.counter);
      this.counter.pause();
  }
}
