import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TeamSignUpComponent } from './team-sign-up/team-sign-up.component';
import { AdvisorSignUpComponent } from './advisor-sign-up/advisor-sign-up.component';
import { StudentSignUpComponent } from './student-sign-up/student-sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { from } from 'rxjs';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

import { AuthGuard } from './auth/auth.guard';

import { LogoComponent} from './logo/logo.component';
import { GamevideosComponent} from './gamevideos/gamevideos.component';
import { ChatComponent} from './chat/chat.component';
import { InviteComponent } from './invite/invite.component';
import { CmsComponent} from './cms/cms.component';
import { MyprofileComponent} from './myprofile/myprofile.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { StageoneoverviewComponent } from './stageoneoverview/stageoneoverview.component';
import { StagesecondoverviewComponent } from './stagesecondoverview/stagesecondoverview.component';
import { StagethirdoverviewComponent } from './stagethirdoverview/stagethirdoverview.component';
import { StagefouroverviewComponent } from './stagefouroverview/stagefouroverview.component';
import { FinalResultsComponent } from './final-results/final-results.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'company-logo',
    component: LogoComponent
  },
  {
    path: 'team-chat',
    component: ChatComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'knowledge-videos',
    component: GamevideosComponent
  },
  {
    path: 'team-signup',
    component: TeamSignUpComponent
  },
  {
    path: 'advisor-signup',
    component: AdvisorSignUpComponent
  },
  {
    path: 'student-signup',
    component: StudentSignUpComponent
  },
  {
    path: 'login',
    component: SignInComponent
  },
  {
    path: 'reset-password/:token',
    component: ResetPasswordComponent
  },
  {
    path: 'forget-password',
    component: ForgetPasswordComponent
  },
  {
    path: 'stage-one-overview',
    component: StageoneoverviewComponent,
     canActivate:[AuthGuard]
  },
  {
    path: 'stage-second-overview',
    component: StagesecondoverviewComponent,
     canActivate:[AuthGuard]
  },
  {
    path: 'stage-third-overview',
    component: StagethirdoverviewComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'stage-four-overview',
    component: StagefouroverviewComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'invites',
    component: InviteComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'final-results',
    component: FinalResultsComponent
  },
  {
    path: 'pages',
      children: [
        { path: ':slug', component: CmsComponent },
    ]
  },
  {
    path: 'user',canActivate: [AuthGuard],
      children: [
        { path: '', component: LogoComponent },
        { path: 'my-profile', component: MyprofileComponent },
        { path: 'change-password', component: ChangepasswordComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
