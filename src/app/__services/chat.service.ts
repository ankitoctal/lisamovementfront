import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Message } from '../message'
import { Observable } from 'rxjs/observable';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private socket: Socket,private http: HttpClient) { }
  sendMessage(msg: string,user_id:string,user_name:string,team_id:string){
      this.socket.emit("message", {message: msg,user_name:user_name,user_id: user_id,team_id: team_id});
  }

  public newMessageReceived() {
    const observable = new Observable<{ user: String, message: String}>(observer => {
      this.socket.on('message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  public receivedTyping() {
    const observable = new Observable<{ isTyping: boolean}>(observer => {
      this.socket.on('typing', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  
  getUserChat(team_id) { 
    return this.http.get(environment.apiUrl+'/get-chat/'+team_id);   		
  } 
}
