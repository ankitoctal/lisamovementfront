import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";

import { User } from '../__models/user';
import { from, Observable } from 'rxjs';

@Injectable()

export class UserService {

    constructor(private http: HttpClient,private router:Router){
    }

    register(user: User) {
        return this.http.post(environment.apiUrl+'/register',user);
    }

}

