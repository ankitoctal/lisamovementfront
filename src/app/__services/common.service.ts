import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  logoDetails() { 
    return this.http.get(environment.apiUrl+'/logo-details');   		
  } 
  
  getGameVideosDetails(catType) { 
    return this.http.get(environment.apiUrl+'/game-videos/'+catType);   		
  }

  getTeamDropdown() { 
    return this.http.get(environment.apiUrl+'/team-list');   		
  } 
  
  getCmsPage(slug){
    return this.http.get(environment.apiUrl+'/pages/'+slug);   		
  }

  gameName(slug){
    return this.http.get(environment.apiUrl+'/game-name/'+slug);   		
  }


  getLeaderboard(game_id:any){
    return this.http.post(environment.apiUrl+'/get-leaderboard',{game_id});
  }  

  updateTimer(time:any,user_id:any){
    return this.http.get(environment.apiUrl+'/update-timer/'+time+'/'+user_id);   		
  }

  checkGameTime(id:any){
    return this.http.get(environment.apiUrl+'/content-manager/view/'+id);   		
  }
	getPlacementDetails(){
    	return this.http.get(environment.apiUrl+'/placement/get-placement-details');   		
	}

	getPlacementCategoryList(){
		return this.http.get(environment.apiUrl+'/get-placement-category-list');
	}

	saveSelectedPlacement(body): Observable<any>{
		return this.http.post(environment.apiUrl+'/save-placement',body);
	}

	getSelectedPlacement(team_id:any,game_id:any){
		return this.http.post(environment.apiUrl+'/get-placements',{team_id,game_id});
	}
	getUser(user_id:any) { 
		return this.http.get(environment.apiUrl+'/users/getuser/'+user_id);   		
	} 

	getGameManagerSetting(slug){
		return this.http.get(environment.apiUrl+'/getGameManagerSetting/'+slug);  
	}

	getSponserLogo(stage:string){
		return this.http.post(environment.apiUrl+'/sponser/',{stage});  
	}
  
}
