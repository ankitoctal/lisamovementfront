import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";

import { User } from '../__models/user';
import { from, Observable } from 'rxjs';

@Injectable()

export class UserService {

    constructor(private http: HttpClient,private router:Router){  }

    register(user: User) {
        return this.http.post(environment.apiUrl+'/register',user);
    }

    login(user: User) {
      return this.http.post<any>(environment.apiUrl+'/login', user)
          .pipe(map(user => {
              return user;
          })
        );
    }

    sendInviteLink(body): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/invite-link', body);
    }


    forgetPassword(email:string) {
      return this.http.post<any>(environment.apiUrl+'/forget-password', {email})
          .pipe(map(user => {
              return user;
          })
        );
    }

    ValidPasswordToken(body): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/verify-token', body);
    }

    resetPassword(body): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/reset-password',body);
    }

    logout() {
      // remove user from local storage to log user out
      let removeToken = localStorage.removeItem('access_token');
      if (removeToken == null) {
        this.router.navigate(['login']);
      }
    }

    getToken() {
      return localStorage.getItem('access_token');
    }

    getCurrentUser() {
      var user = localStorage.getItem('currentUser');
      return user ? JSON.parse(user): [];
    }

    isLoggedIn(): boolean {
      let authToken = localStorage.getItem('access_token');
      return authToken ? true : false;
    }
    saveUser(user) { 
      return this.http.post(environment.apiUrl+'/users/updateUser',user);   		
    }
    
    changePassword(user) { 
    return this.http.post(environment.apiUrl+'/users/changepassword',user);   		
    }
}

