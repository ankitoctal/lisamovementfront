import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";

import { GameStageDetail } from '../__models/game-stage-detail';
import { from, Observable } from 'rxjs';

@Injectable()

export class GameStageService {

    constructor(private http: HttpClient,private router:Router){  }

    gameStageDetail(stage:string,gameId:string) {
        return this.http.post(environment.apiUrl+'/get-stage-detail',{stage,gameId});
    }

    gameStageResearchDetail(stage:string,gameId:string) {
        return this.http.post(environment.apiUrl+'/get-stage-research',{stage,gameId});
    }

    getTeamList(team_id:string){
    	return this.http.post(environment.apiUrl+'/get-team-list',{team_id});
    }

    getStageLeaderList(team_id:string){
    	return this.http.post(environment.apiUrl+'/get-stage-list',{team_id});
    }

    getAssignLeader(body): Observable<any>{
        return this.http.post(environment.apiUrl+'/assign-leader', body);
    }

    getTeamMemberCount(team_id:string){
        return this.http.post(environment.apiUrl+'/members-count', {team_id});
    }

    saveWorkforce(body): Observable<any>{
        return this.http.post(environment.apiUrl+'/save-workforce', body);
    }

    getWorkforce(team_id:string){
        return this.http.post(environment.apiUrl+'/get-workforce', {team_id});
    }

    getContractorList(){
        return this.http.get(environment.apiUrl+'/get-contractors');
    }

    getContractor(id:string){
        return this.http.post(environment.apiUrl+'/get-contractor',{id});
    }

    getGameDetails(){
        return this.http.get(environment.apiUrl+'/game-detail');
    }

    saveStageResult(body): Observable<any>{
        return this.http.post(environment.apiUrl+'/save-score', body);
    }

    getTeamStageStatus(stage:string,game_id:string,team_id:string){
        return this.http.post(environment.apiUrl+'/team-stage-status', {stage,game_id,team_id});
    }

    saveTeamStageStatus(body): Observable<any>{
        return this.http.post(environment.apiUrl+'/save-stage-status', body);
    }

    getCompletedStage(team_id:string,game_id:string){
        return this.http.post(environment.apiUrl+'/get-complete-stage',{team_id,game_id});
    }

    getScoreDetails(team_id:string,game_id:string,stage:string){
        return this.http.post(environment.apiUrl+'/get-score-details',{team_id,game_id,stage});
    }

}