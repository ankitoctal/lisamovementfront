import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";
import { from, Observable } from 'rxjs';

@Injectable()

export class GameQuestionService {

    constructor(private http: HttpClient,private router:Router){  }

    gameIssueQuestion(stage:string) {
        return this.http.post(environment.apiUrl+'/get-stage-issue',{stage});
    }

    saveIssueQuestion(body): Observable<any>{
    	return this.http.post(environment.apiUrl+'/save-issue-result',body);
    }

    getQuestionResult(team_id:string, game_id:string, stage:string){
    	return this.http.post(environment.apiUrl+'/get-issue-result',{team_id,game_id,stage});
    }


}