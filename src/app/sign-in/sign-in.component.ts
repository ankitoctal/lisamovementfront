import { Component, OnInit } from '@angular/core';
import { FormGroup , Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { CustomValidation } from '../__helpers/custom-validation.validators';
import { UserService } from '../__services/user.service';
import { GameStageService } from '../__services/game-stage.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: []
})
export class SignInComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  resposne = [];
  errorMsg:any={};


  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private gameStageService: GameStageService,
    private router: Router,
    private toastr:ToastrService) { }

  ngOnInit() {
      if(localStorage.getItem('access_token')) {
        this.router.navigate(['home']);
      }

      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required,Validators.email]],
        password: ['', [Validators.required, Validators.minLength(4)]]
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  login() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.userService.login(this.loginForm.value).subscribe(
      (data:any) => {
        if(data['status'] == 1) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('currentUser', JSON.stringify(data['user']));
              this.toastr.success(data['message']);
              localStorage.setItem('access_token', data.user.token);

              if(data.user.position == 1){
                this.gameStageService.getTeamMemberCount(data.user.team_id).subscribe(
                  (response:any) => {
                    if(response.status == 1 && response.totalMember != 4) {
                        this.router.navigate(['invites']);
                    }
                  })
              }

              this.router.navigate(['stages']);
        } else if(data['status'] == 0) {
            this.toastr.error(data['message']);
        } else {
            this.toastr.error(data['message']);
        }
      },
      error =>{
        this.toastr.error(error.message);
      }
    )
  }

}
