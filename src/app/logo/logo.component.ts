import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { CommonService } from '../__services/common.service';
import { environment } from "../../environments/environment";
import { Title}  from '@angular/platform-browser';
@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {
  public details : any = [];
  env = environment;
  constructor(private commonService: CommonService,private activatedRoute: ActivatedRoute,private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle(environment.siteName + ' - Logo & company');
    this.activatedRoute.params.subscribe(routeParams => {
      this.commonService.logoDetails().subscribe((response: any) => {
        this.details = response.data; 
        console.log(this.details);
      });
    });
  }

}
