import { Component, OnInit } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { UserService } from '../__services/user.service';
import { CommonService } from '../__services/common.service';
import { GameStageService } from '../__services/game-stage.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-stagesecondoverview',
	templateUrl: './stagesecondoverview.component.html',
	styleUrls: ['./stagesecondoverview.component.css']
})

export class StagesecondoverviewComponent implements OnInit {

	gameDetail:any;
	stage:any = 2;
	leaderName:any = '';
	user:any;
	checkLeader:any;
	teamName:any;
	stage_leader:any;
	users:any = [];
	completedStages:any;
	stageSecond:any = false;

	constructor(
		private titleService: Title,
		public commonService:CommonService,
		public userService:UserService,
		private toastr : ToastrService,
		private gameStageService : GameStageService
	) { }

	async ngOnInit() {
		this.user = await this.userService.getCurrentUser();

		this.titleService.setTitle(environment.siteName + ' - Stage One Overview');

		var n = this.user.stage_leader.includes(5);

		await this.gameStageService.getTeamList(this.user.team_id).subscribe(
          	(data:any) => {
              	if(data['status'] == 1) {
                	this.users = data.userList;
                	this.completedStage(this.user.team_id);
            	}
          	},
          	error =>{
            	this.toastr.error(error.message);
          	}
      	)
	}

	async completedStage(teamId){
		
  		await this.gameStageService.getGameDetails().subscribe( (data:any) => {

                if(data['status'] == 1) {
                    this.gameDetail = data.detail;

                      // get complete or pause
                    localStorage.setItem('gameId', this.gameDetail._id);
                    
                    this.gameStageService.getCompletedStage(teamId,this.gameDetail._id).subscribe((data:any) => {

			              	if(data['status'] == 1) {
			              		this.completedStages =  data.detail.find(el => el.stage == this.stage);
			              		this.stageSecond = true;
			            	}
			          	},
			          	error =>{
			            	this.toastr.error(error.message);
			          	}
			      	)
                }
            },
            error =>{
                  this.toastr.error(error.message);
              }
          );		
  	} 

	getUser(stage){
		for (var i = 0; i < this.users.length;  i++) {
			let userDetails = this.users[i].stage_leader.find(el => el === stage);
			if (userDetails) { return this.users[i].first_name+' '+this.users[i].last_name;	}
		}
	}

}
