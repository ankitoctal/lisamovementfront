import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomValidation } from './__helpers/custom-validation.validators';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { StagesModule } from './stages/stages.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { TeamSignUpComponent } from './team-sign-up/team-sign-up.component';
import { AdvisorSignUpComponent } from './advisor-sign-up/advisor-sign-up.component';
import { StudentSignUpComponent } from './student-sign-up/student-sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HttpClientModule } from '@angular/common/http';
import { PipeModule } from './pipe/pipes.module';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment';
/*
* service
*/

import { UserService} from './__services/user.service';
import { GameStageService} from './__services/game-stage.service';
import { GameQuestionService} from './__services/game-question.service';
import { from } from 'rxjs';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LogoComponent } from './logo/logo.component';
import { GamevideosComponent } from './gamevideos/gamevideos.component';

import { ChatComponent } from './chat/chat.component';

import { InviteComponent } from './invite/invite.component';
import { DateAgoPipe } from './pipe/pipes/date-ago.pipe';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { CmsComponent } from './cms/cms.component';
import { GameheaderComponent } from './gameheader/gameheader.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ProfilesidebarComponent } from './profilesidebar/profilesidebar.component';
import { StageoneoverviewComponent } from './stageoneoverview/stageoneoverview.component';
import { StagesecondoverviewComponent } from './stagesecondoverview/stagesecondoverview.component';
import { StagethirdoverviewComponent } from './stagethirdoverview/stagethirdoverview.component';
import { StagefouroverviewComponent } from './stagefouroverview/stagefouroverview.component';
import { FinalResultsComponent } from './final-results/final-results.component';


const config: SocketIoConfig = { url: environment.serverUrl, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    TeamSignUpComponent,
    AdvisorSignUpComponent,
    StudentSignUpComponent,
    SignInComponent,
    ResetPasswordComponent,
    ForgetPasswordComponent,
    LogoComponent,
    GamevideosComponent,
    ChatComponent,
    InviteComponent,
    SafeHtmlPipe,
    DateAgoPipe,
    CmsComponent,
    GameheaderComponent,
    MyprofileComponent,
    ChangepasswordComponent,
    ProfilesidebarComponent,
    StageoneoverviewComponent,
    StagesecondoverviewComponent,
    StagethirdoverviewComponent,
    StagefouroverviewComponent,
    FinalResultsComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StagesModule,
    PipeModule,
    ToastrModule.forRoot({
      timeOut:2000
    }),
    SocketIoModule.forRoot(config)
  ],
  providers: [
    CustomValidation,
    UserService,
    GameStageService,
    GameQuestionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
