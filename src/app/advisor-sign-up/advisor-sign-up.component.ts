import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,ValidatorFn,ValidationErrors  } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidation } from '../__helpers/custom-validation.validators';
import { UserService } from '../__services/user.service';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../__services/common.service';

@Component({
  selector: 'app-advisor-sign-up',
  templateUrl: './advisor-sign-up.component.html',
  styleUrls: []
})

export class AdvisorSignUpComponent implements OnInit {

    registerForm: FormGroup;
    loading = false;
    submitted = false;
    resposne = [];
    errorMsg:any={};
    teamList = [];

    constructor(
        private formBuilder: FormBuilder,
        private matchConfirmPassword: CustomValidation,
        private userService: UserService,
        private router: Router,
        private toastr:ToastrService,
        private commonService: CommonService
    ) { }

    ngOnInit() {
        if(localStorage.getItem('access_token')) {
          this.router.navigate(['home']);
        }
        this.commonService.getTeamDropdown().subscribe((response: any) => {
          this.teamList = response.data;    
        });
        this.registerForm = this.formBuilder.group({
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          teamName: [''],
          position: [],
          team_id: [''],
          role_id: [],
          email: ['', [Validators.required,Validators.email]],
          password: ['', [Validators.required, Validators.minLength(4)]],
          confirmPassword: ['', [Validators.required, Validators.minLength(4)]]
      },
      {
        validator: [this.matchConfirmPassword.mustMatch('password', 'confirmPassword'),atLeastOne(Validators.required, ['teamName','team_id'])],
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    registerUser() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
          return;
        }

        this.loading = true;

        this.userService.register(this.registerForm.value).subscribe(
          (data:any) => {
            if(data.status == 2){
              if(data.error.firstName){
                this.errorMsg = data.error.firstName.message;
              }
              if(data.error.lastName){
                this.errorMsg = data.error.lastName.message;
              }
              if(data.error.email){
                this.errorMsg = data.error.email.message;
              }
              if(data.error.teamName){
                this.errorMsg = data.error.teamName.message;
              }

              if(data.error.password){
                this.errorMsg = data.error.password.message;
              }
              if(data.error.confirmPassword){
                this.errorMsg = data.error.confirmPassword.message;
              }

              this.toastr.error(this.errorMsg,'error');
            }else if(data.status == 1){

              this.toastr.success(data.message,'success');
              this.router.navigate(['login']);
            } else {
                this.toastr.error(data['message']);
            }
          },
          error =>{
            this.toastr.error(error,'error');
          }
        )
    }
}
export const atLeastOne = (validator: ValidatorFn, controls:string[] = null) => (
  group: FormGroup,
): ValidationErrors | null => {
  if(!controls){
    controls = Object.keys(group.controls)
  }

  const hasAtLeastOne = group && group.controls && controls
    .some(k => !validator(group.controls[k]));

  return hasAtLeastOne ? null : {
    atLeastOne: true,
  };
};