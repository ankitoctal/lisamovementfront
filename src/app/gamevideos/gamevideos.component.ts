import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { CommonService } from '../__services/common.service';
import { environment } from "../../environments/environment";
import { Title}  from '@angular/platform-browser';
import { UserService } from '../__services/user.service';

@Component({
  selector: 'app-gamevideos',
  templateUrl: './gamevideos.component.html',
  styleUrls: ['./gamevideos.component.css']
})


export class GamevideosComponent implements OnInit {
  public detailsCat1 : any = [];
  public detailsCat2 : any = [];
  public detailsCat3 : any = [];
  public detailsCat4 : any = [];
  env = environment;

  constructor(
      private commonService: CommonService,
      private activatedRoute: ActivatedRoute,
      private titleService: Title,
      public userService: UserService) {
      
      }


  ngOnInit(): void {
  
    this.titleService.setTitle(environment.siteName + '- knowledge-videos');

    this.activatedRoute.params.subscribe(routeParams => {
      this.commonService.getGameVideosDetails(0).subscribe((response: any) => {
        this.detailsCat1 = response.data; 
      });
      this.commonService.getGameVideosDetails(1).subscribe((response: any) => {
        this.detailsCat2 = response.data; 
      });
      this.commonService.getGameVideosDetails(2).subscribe((response: any) => {
        this.detailsCat3 = response.data; 
      });
      this.commonService.getGameVideosDetails(3).subscribe((response: any) => {
        this.detailsCat4 = response.data; 
      });
    });
  }
  

}
