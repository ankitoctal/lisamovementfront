import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamevideosComponent } from './gamevideos.component';

describe('GamevideosComponent', () => {
  let component: GamevideosComponent;
  let fixture: ComponentFixture<GamevideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamevideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamevideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
