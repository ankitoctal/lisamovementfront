import { Component, OnInit } from '@angular/core';
import { CommonService } from '../__services/common.service';
import { Router,ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.css']
})
export class CmsComponent implements OnInit {
  data:any={};
  slug:String;
  constructor(private commonService: CommonService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(routeParams => {
      this.slug = routeParams.slug;
      this.commonService.getCmsPage(routeParams.slug).subscribe((response: any) => {
        this.data = response.data;                   
      });
    });
  }

}
