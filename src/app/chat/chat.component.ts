import { Component, OnInit,ElementRef,ViewChild,AfterViewChecked } from '@angular/core';
import { ChatService  } from '../__services/chat.service';
import { UserService } from '../__services/user.service';
import { CommonService } from '../__services/common.service';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit,AfterViewChecked  {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  newMessage: string;
  senderId: string;
  user_id: string;
  user_name: string;
  team_id: string;
  messageList:  string[] = [];
  public details : any = [];
  public contentSettings : any = {};
  messageArray: Array<{user: String, message: String}> = [];
  isTyping = false;

    constructor(
        private chatService: ChatService,
        public userService:UserService,
        private commonService: CommonService
    ) {
        this.chatService.newMessageReceived().subscribe(data => { 
            this.messageArray.push(data);
            console.log(this.messageArray);
            this.isTyping = false;
        });

        this.chatService.receivedTyping().subscribe(bool => {
            this.isTyping = bool.isTyping;
        });
    }
  
    sendMessage() {
        console.log(this.newMessage);
        this.chatService.sendMessage(this.newMessage,this.user_id,this.user_name,this.team_id);
        this.newMessage = ''; 
    }

    typingMessage(){
        this.chatService.receivedTyping().subscribe(bool => {
            this.isTyping = bool.isTyping;
        });
    }

    ngOnInit() { 
        this.scrollToBottom();
        var user = localStorage.getItem('currentUser');
        var userDetails =  user ? JSON.parse(user): [];
        this.user_id = userDetails.user_id;
        this.user_name = userDetails.name;
        this.team_id = userDetails.team_id;
        this.chatService.getUserChat(this.team_id).subscribe((response: any) => {
            this.messageArray = response.data;
        });

        this.commonService.gameName('main-page').subscribe((response: any) => {
            this.contentSettings = response.data; 
        });
    } 
    ngAfterViewChecked() {        
        this.scrollToBottom();        
    } 
    scrollToBottom() {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }                 
  }

}
