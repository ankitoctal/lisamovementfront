export class Message {
    _id: string;
    message: string;
    sender: string;
}