import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title }  from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { UserService } from '../__services/user.service';
import { CommonService } from '../__services/common.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {
  user :any;
  userForm: any = {};  
  constructor(private titleService: Title,
            public userService:UserService,
            public commonService:CommonService,
            private router: Router,
            public toastr : ToastrService,
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle(environment.siteName + ' - My Profile');	
  		this.user = this.userService.getCurrentUser();	  		
  		this.commonService.getUser(this.user.user_id).subscribe((response:any) => { 
	  		this.userForm = response.data;	  		
	    });	
  }

  public submitForm(){
    this.userService.saveUser(this.userForm).subscribe(response => {   
      this.toastr.success(response['msg']);    		
      this.router.navigate(['/my-profile']);	
    });
  }

}
