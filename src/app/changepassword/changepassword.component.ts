import { Component, OnInit } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import {Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../__services/user.service';
import { ToastrService } from 'ngx-toastr'; 
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  
  env = environment;
  user;
  userForm: any = {};  	
  constructor(private titleService: Title,public userService:UserService,public toastr : ToastrService,private router: Router) { }

  ngOnInit(): void {
    this.titleService.setTitle(environment.siteName + ' - Change Password');

  		this.userForm = new FormGroup({
  			'id': new FormControl(''), 
  			'old_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]),     		
      		'password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]), 
      		'confirm_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        		        
      		]),     		
      },ValidateConfirmPassword);	
      this.user = this.userService.getCurrentUser();    
  }
  public submitForm(){  		

    if (this.userForm.valid) {
      this.userForm.value.id = this.user.user_id;        	
        this.userService.changePassword(this.userForm.value).subscribe(response => { 
          if(response['status']=='success')      		
          { 
            this.toastr.success(response['msg']);    
            this.router.navigate(['change-password']);	
        }
        else
        {
          this.toastr.error(response['msg']); 
        }	
      });
      } else {
        this.validateAllFormFields(this.userForm);
      }
        
      
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
} 