import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup , Validators, FormBuilder, FormArray } from '@angular/forms';
import { GameStageService } from '../../__services/game-stage.service';
import { CommonService } from '../../__services/common.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-secondstageresult',
	templateUrl: './secondstageresult.component.html',
	styleUrls: [],
	providers: [DatePipe]
})

export class SecondstageresultComponent implements OnInit {
	

	gameId:string = "";
	currentUser:any = {};
    placementDetail:any;
    savedMinute:number = 0;
    gameStage:string = "2";
    result:any = {};

    total:number;

    saveAmount:number = 0;

    public now: Date = new Date();

  	constructor(
    	private commonService : CommonService,
  		private gameStageService : GameStageService,
        private toastr : ToastrService,
	    private formBuilder: FormBuilder,
	    private router: Router,
        public datepipe: DatePipe) { 
        	var user = localStorage.getItem('currentUser');
		    this.currentUser = JSON.parse(user);
        }

	async ngOnInit() {

		this.gameId = localStorage.getItem('gameId');

		// per min saving amount 
		this.saveAmount = parseFloat(localStorage.getItem('saving_min'));
		var seconds:any;

		
		if (localStorage.getItem("timer") != null) {
			const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
			const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

			let date1:any = new Date(currentTime); 
	        let date2:any = new Date(endtime);

	        seconds = (date2 - date1)/1000;
	        this.savedMinute = Math.floor(seconds / 60);
		}

		await this.getPlacementResult();
		
        localStorage.removeItem('endtime');
        localStorage.removeItem('timer');
	}

	async getPlacementResult(){
		await this.commonService.getSelectedPlacement(this.currentUser.team_id,this.gameId).subscribe(
	            (data:any) => {
	            	if(data['status'] == 1) {

	            		this.placementDetail = data.detail;

	            		this.total = this.getTotalBonus(data.detail);

	                    this.result.team_id = this.currentUser.team_id;
	                    this.result.game_id = this.gameId;
	                    this.result.score = (this.saveAmount * this.savedMinute) + this.total;
	                    this.result.savedMin = this.savedMinute;
	                    this.result.stage = "2";
	                    this.result.status = "Complete";
	                    this.result.min_price = this.saveAmount;
	                    this.result.total_spent =  0;

	                    
	                    this.saveResult(this.result);
	            	}
	            },
	            error =>{
	                this.toastr.error(error.message);
	            }
	        )
	}

	async saveResult(resultData){
		await this.gameStageService.saveStageResult(resultData).subscribe(
	                        (data:any) => {},
	                        error =>{ this.toastr.error(error.message); }
                    	);
	}

	getTotalBonus(details){
		let totalBonus:number = 0;

		if (details.crane.answer == 'Good') { totalBonus += 100000;}
	
		if (details.crane.answer == 'Better') { totalBonus += 200000; }
	
		if (details.crane.answer == 'Best') { totalBonus += 300000; }
		
		if (details.trailer.answer == 'Good') { totalBonus += 100000; }
		
		if (details.trailer.answer == 'Better') { totalBonus += 200000; }
		
		if (details.trailer.answer == 'Best') { totalBonus += 300000; }
		
		if (details.ingress.answer == 'Good') { totalBonus += 100000; }
		
		if (details.ingress.answer == 'Better') { totalBonus += 200000; }
		
		if (details.ingress.answer == 'Best') { totalBonus += 300000; }
		
		if (details.material.answer == 'Good') { totalBonus += 100000; }
		
		if (details.material.answer == 'Better') { totalBonus += 200000; }
		
		if (details.material.answer == 'Best') { totalBonus += 300000; }
		
		return totalBonus;
	}

}
