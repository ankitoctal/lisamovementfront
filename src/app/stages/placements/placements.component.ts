import { Component, OnInit, ViewChild } from '@angular/core';
import { Title}  from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../__services/common.service';
import { GameStageService } from '../../__services/game-stage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CountdownComponent } from 'ngx-countdown';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-placements',
  templateUrl: './placements.component.html',
  styleUrls: ['./placements.component.css'],
  providers: [DatePipe]

})

export class PlacementsComponent implements OnInit {

	@ViewChild('cd') counter: CountdownComponent;

	gameStage:string = "2";
	gameId:string = "";
	currentUser:any = {};
	current_route:string = "";
	teamStageStatus:any = {};
	selectedPlacement:any;

	details:any;
	categoryList:any;
	statusCran: any;
	statusMat: any;
	statusCommand:any;
	statusIngress: any;
	selected :any;

	pause:any;
    saveAmount:number = 0;
    seconds:any = "3600";
    remainingTime: number = 3600;

    sponser:any;
    env = environment;

    
    public now: Date = new Date();
    public newDate: Date = new Date();
	
	constructor(
    	private commonService : CommonService,
    	private gameStageService : GameStageService,
    	public toastr : ToastrService,
        private activatedRoute : ActivatedRoute,
        private router: Router,
        public datepipe: DatePipe
	) { 
		var user = localStorage.getItem('currentUser');
        this.currentUser = JSON.parse(user); 
    }

	ngOnInit() {

		this.gameId = localStorage.getItem('gameId');
        this.current_route = this.activatedRoute.snapshot['_routerState'].url;

        this.saveAmount = parseFloat(localStorage.getItem('saving_min'));
        
        this.storeTimer();

        this.pause = JSON.parse(localStorage.getItem('stagepause'));
        
        if (this.pause) { this.counter.pause(); }

        this.commonService.getSponserLogo(this.gameStage).subscribe(
            (response:any) => {
                    if(response['status'] == 1) {
                        this.sponser = response.data;
                    }
                },
                error =>{
                    this.toastr.error(error.message);
                }
            )


    	this.commonService.getPlacementDetails().subscribe((response: any) => {
      		this.details = response.data;
    	});
    
    	this.commonService.getPlacementCategoryList().subscribe((response: any) => {
    		this.categoryList = response.data;
    	});
  	}

	clickEventMat(item){
	    this.statusMat = item;
	}

	isEventMat(item) {
	    return this.statusMat === item;
	};
	 
	clickEventCommand(item:any){
	   this.statusCommand = item; 
	}

	isEventCommand(item:any) {
	    return this.statusCommand === item;
	};

	clickEventIngress(item){
		this.statusIngress = item; 
	}

	isEventIngress(item:any) {
		return this.statusIngress === item;
	};

	select(item) {
		this.statusCran = item; 
	};

	isActive(item) {
		return this.statusCran === item;
	};


	submitResult(){

		if(this.statusCran === undefined){
			this.toastr.error('Please Select Crane.');
		} else if(this.statusIngress === undefined){
			this.toastr.error('Please Select Ingress/Egress.');
		} else if(this.statusCommand === undefined){
			this.toastr.error('Please Select Command Trailer.');
		} else if(this.statusMat === undefined){
			this.toastr.error('Material Stockpile.');
		} else {

			this.selectedPlacement = {
	            'team_id':this.currentUser.team_id,
	            'game_id': this.gameId,
	            'material': this.statusMat,
	            'crane':this.statusCran,
	            'trailer':this.statusCommand,
	            'ingress':this.statusIngress
	        }

	    	this.commonService.saveSelectedPlacement(this.selectedPlacement).subscribe(
	            (data:any) => {
	                if(data['status'] == 1) {
	                    this.toastr.success(data['message']);
	                    this.router.navigate(['stages/second-stage-result']);
	                    
	                } else if(data['status'] == 0) {
	                    this.toastr.error(data['message']);
	                } else {
	                    this.toastr.error(data['message']);
	                }
	            },
	            error =>{
	                this.toastr.error(error.message);
	            }
	        )
		}
		
	}


	storeTimer(){
        localStorage.removeItem('timer');
        const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
        const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(currentTime); 
        let date2:any = new Date(endtime);

        this.seconds = (date2 - date1)/1000;
    
        localStorage.setItem('timer', this.seconds);

        this.remainingTime =  parseFloat(this.seconds);
    }

    storeTeamStageStatus(){
       
        this.pause = true;
        localStorage.setItem('stagepause',this.pause);
        this.counter.pause();
       
        let currentDateTime: Date = new Date();

        const dateA = this.datepipe.transform(currentDateTime, 'yyyy-MM-dd HH:mm:ss');
        const dateB = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(dateA); 
        let date2:any = new Date(dateB);

        this.seconds = (date2 - date1)/1000;

        this.teamStageStatus = {
            'team_id':this.currentUser.team_id,
            'game_id': this.gameId,
            'stage': this.gameStage,
            'remaining_time':this.seconds,
            'current_route':this.current_route
        }

        this.gameStageService.saveTeamStageStatus(this.teamStageStatus).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                } else if(data['status'] == 0) {
                    this.toastr.error(data['message']);
                } else {
                    this.toastr.error(data['message']);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )
    }

    resumeStage(){

        let resumeDateTime: Date = new Date();

        this.gameStageService.getTeamStageStatus(this.gameStage, this.gameId,this.currentUser.team_id).subscribe(
                            (data:any) => {
                                // status 3 = stage complete and score get ,2 = required parameter missing, 1 = stage pause by team , 0 = start stage
                                if(data.status == 3) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);
                                }else if(data.status == 2) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);  
                                }else if(data.status == 1) {

                                    this.pause = false;
                                    localStorage.setItem('stagepause',this.pause);
                                    
                                    resumeDateTime.setSeconds(resumeDateTime.getSeconds() + JSON.parse(data.detail.remaining_time));
                                    
                                    let endDatetime = this.datepipe.transform(resumeDateTime, 'yyyy-MM-dd HH:mm:ss');
                                    
                                    localStorage.setItem('endtime', endDatetime);

                                    localStorage.setItem('timer', data.detail.remaining_time);
                             
                                    this.remainingTime =  parseFloat(data.detail.remaining_time);
                                    this.counter.resume();
                                }

                            },
                            error =>{
                                this.toastr.error(error.message);
                                this.router.navigate(['stages']);
                            }
                        );
    }


	// For material checkbox
 
}
