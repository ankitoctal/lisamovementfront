import { Component, OnInit } from '@angular/core';
import { FormGroup , Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { GameStageService } from '../../__services/game-stage.service';
import { CommonService } from '../../__services/common.service';
import { UserService } from '../../__services/user.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
	selector: 'app-stage-team',
	templateUrl: './team.component.html',
	styleUrls: []
})

export class TeamComponent implements OnInit {
  
	assignLeaderForm: FormGroup; 
	submitted = false;
	stageFirst = false;
	stageSecond = false;
	stageThird = false;
	stageFour = false;
	currentUser:any = {};
	team_Id:string;
	user_id:string;
	userPosition:number = 0;
	users:any= [];
	teamName:any= {};
	stages:any= [];
	team:any= {};
	gameDetail:any= {};
	completedStages:any;
	stage_leader:any;
	scoreDetail:any;
	answerDetail:any;

	stage:any = 1;
	sponser:any;
	env = environment;

  constructor(
  	private commonService: CommonService,
    private gameStageService : GameStageService,
    private toastr : ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router ) {
    	var user = localStorage.getItem('currentUser');
    	this.currentUser = JSON.parse(user);
    	this.userPosition = this.currentUser.position;
    	this.stage_leader = this.currentUser.stage_leader;
    }

	async ngOnInit() {
    	await this.getTeamList(this.currentUser.team_id);
    	this.commonService.getSponserLogo(this.stage).subscribe(
			(response:any) => {
					if(response['status'] == 1) {
						this.sponser = response.data;
					}
				},
				error =>{
					this.toastr.error(error.message);
				}
			)
  	}

  	async getTeamList(teamId){
    	this.assignLeaderForm = this.formBuilder.group({
            stage1: [''],
            stage2: [''],
            stage3: [''],
            stage4: [''],
            user_id: [''],
            team_Id: ['']
        });
    
      	await this.gameStageService.getTeamList(teamId).subscribe(
          	(data:any) => {
              	if(data['status'] == 1) {
                	this.users = data.userList;
                	var sd = this.currentUser.user_id;
                	
                	let userDetails = this.users.find(el => el._id === this.currentUser.user_id);
                
                    this.stage_leader = userDetails.stage_leader;
                 
                	this.teamName = data.team;

                	this.completedStage(teamId);
                    
            	} else if(data['status'] == 0) {
                	this.toastr.error(data['message']);
            	} else {
                	this.toastr.error(data);
            	}
          	},
          	error =>{
            	this.toastr.error(error.message);
          	}
      	)
  	}

  	async completedStage(teamId){

  		await this.gameStageService.getGameDetails().subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.gameDetail = data.detail;
                      // get complete or pause
                    localStorage.setItem('gameId', this.gameDetail._id);
                    
                    this.gameStageService.getCompletedStage(teamId,this.gameDetail._id).subscribe(
			          	(data:any) => {
			              	if(data['status'] == 1) {
			              		this.completedStages = data.detail;

			              		if (data.lastCompletedStage == 1) {
			              			this.stageSecond = this.stage_leader.find(function(el){ return el === 2});
			              		}else if (data.lastCompletedStage == 2) {
			              			this.stageThird = this.stage_leader.find(function(el){ return el === 3});
			              			
			              		} else if (data.lastCompletedStage == 3) {
			              			this.stageFour = this.stage_leader.find(function(el){ return el === 4});
			              		} else{
			              			this.toastr.error('You already completed Your Stage.');
			              		}
			            	} else if(data['status'] == 0) {

			                	this.stageFirst = this.stage_leader.find(function(el){ return el === 1});
			            	} else {
			                	this.toastr.error(data);
			            	}
			          	},
			          	error =>{
			            	this.toastr.error(error.message);
			          	}
			      	)
                }
            },
            error =>{
                  this.toastr.error(error.message);
                  this.router.navigate(['stages']);
              }
          );		
  	} 

  	showModal(userId,teamId) {
    	this.user_id = userId;
    	this.team_Id = teamId;
    	// get stage list for assign leader 
      	this.gameStageService.getStageLeaderList(teamId).subscribe(
          (data:any) => {
              if(data['status'] == 1) {
                this.stages = data.stageLeader;
              } else if(data['status'] == 0) {
                  this.toastr.error(data['message']);
              } else {
                  this.toastr.error(data['message']);
              }
          },
          error =>{
            this.toastr.error(error.message);
          }
      )

      $("#stageLeader").modal('show');
      // communication to show the modal, I use a behaviour subject from a service layer here
  	}

  	assignLeader() {
    	this.submitted = true;

    	this.gameStageService.getAssignLeader(this.assignLeaderForm.value).subscribe(
        		(data:any) => {
          			if(data['status'] == 1) {
              			this.toastr.success(data['message']);
              			$("#stageLeader").modal('hide');
              			this.getTeamList(this.currentUser.team_id);
          			} else if(data['status'] == 0) {
              			this.toastr.error(data['message']);
          			} else {
              			this.toastr.error(data['message']);
          			}
        	},
        error =>{
          this.toastr.error(error.message);
        }
      )
  	}

  	getUser(stage){
  		for (var i = 0; i < this.users.length;  i++) {
  			let userDetails = this.users[i].stage_leader.find(el => el === stage);
  			if (userDetails) { return this.users[i].first_name+' '+this.users[i].last_name;	}
  			
  		}
  	}

  	getStageDetail(stage,type){
  		
  		let stageDetails = this.completedStages.find(el => el.stage == stage);
  		
  		if (stageDetails && type == 'score') {return stageDetails.score;}

  		if (stageDetails && type == 'status') {return stageDetails.status;}

  	}

  	getScoreData(stage){
  		var content = $('#stage'+stage);
  		this.gameStageService.getScoreDetails(this.currentUser.team_id,this.gameDetail._id,stage).subscribe(
        		(data:any) => {
          			if(data['status'] == 1) {
          				this.scoreDetail = data.detail; 
                  		this.answerDetail = data.answer; 
          				content.slideToggle('500');
          			} else if(data['status'] == 0) {
              			this.toastr.error(data['message']);
          			} else {
              			this.toastr.error(data['message']);
          			}
        	},
        error =>{
        	this.toastr.error(error.message);
        });
  	}

}
