import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup , Validators, FormBuilder, FormArray } from '@angular/forms';
import { GameStageService } from '../../__services/game-stage.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector:'app-stage-result',
  templateUrl:'./result.component.html',
  styleUrls:[],  
  providers: [DatePipe]
})

export class ResultComponent implements OnInit {

	currentUser:any = {}; 
    result:any = {}; 
    gameDetail:any = {};
    savedMinute:number = 0;
    gameStage:string = "2";

    public now: Date = new Date();

    constructor (
    	private gameStageService : GameStageService,
        private toastr : ToastrService,
	    private formBuilder: FormBuilder,
	    private router: Router,
        public datepipe: DatePipe) {
	    	var user = localStorage.getItem('currentUser');
		    this.currentUser = JSON.parse(user);
	    }

    ngOnInit(){

    	const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
        const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(currentTime); 
        let date2:any = new Date(endtime);

        var seconds:any = (date2 - date1)/1000;

		this.savedMinute = Math.floor(seconds / 60);

        this.gameStageService.getGameDetails().subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.gameDetail = data.detail;
                    this.result.team_id = this.currentUser.team_id;
                    this.result.game_id = this.gameDetail._id;
                    this.result.score = this.gameDetail.saving_amount * this.savedMinute;
                    this.result.savedMin = this.savedMinute;
                    this.result.stage = "1";
                    this.result.status = "Complete";
                    this.result.min_price = this.gameDetail.saving_amount;
                    this.result.total_spent =  JSON.parse(localStorage.getItem('totalCost'))*JSON.parse(this.gameDetail.floor);
                    this.gameStageService.saveStageResult(this.result).subscribe(
                        (data:any) => {
                            if(data['status'] == 1) {
                                console.log(data);
                            }
                         },
                        error =>{
                            this.toastr.error(error.message);
                        }
                    );

                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        );
     

        localStorage.removeItem('endtime');
        localStorage.removeItem('timer');
    }

}
