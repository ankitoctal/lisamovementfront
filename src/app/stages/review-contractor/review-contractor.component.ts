import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup , Validators, FormBuilder, FormArray } from '@angular/forms';
import { GameStageService } from '../../__services/game-stage.service';
import { CommonService } from '../../__services/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CountdownComponent } from 'ngx-countdown';
import { environment } from '../../../environments/environment';

@Component({
  selector:'app-stage-review-contractor',
  templateUrl:'./review-contractor.component.html',
  styleUrls:[],
  providers: [DatePipe]
})

export class ReviewContractorComponent implements OnInit {

    @ViewChild('cd') counter: CountdownComponent;

    gameStage:string = "1";
    gameId:string = "";
    current_route:string = "";s
    avgspeed:any;
    avgquality:any;
    avgsafety:any;
    totalcost:any;
    totalcontent:number = 0;

	saveWorkForceForm: FormGroup;
	currentUser:any = {}; 
    contractorDetail:any = {};
    gameDetail:any = {};
    teamStageStatus:any = {};

    pause:any = false;
    saveAmount:number = 0;
    seconds:any = "3600";
    remainingTime: number = 3600;

    sponser:any;
    env = environment;

    public now: Date = new Date();

    constructor (
        private commonService: CommonService,
	    private gameStageService : GameStageService,
        private toastr : ToastrService,
        private activatedRoute : ActivatedRoute,
	    private formBuilder: FormBuilder,
	    private router: Router,
        public datepipe: DatePipe) {
	    	var user = localStorage.getItem('currentUser');
		    this.currentUser = JSON.parse(user);
	    }

    ngOnInit() {

        this.gameId = localStorage.getItem('gameId');
        this.current_route = this.activatedRoute.snapshot['_routerState'].url;

        this.saveAmount = parseFloat(localStorage.getItem('saving_min'));

        this.storeTimer();

        this.pause = JSON.parse(localStorage.getItem('stagepause'));

        if (this.pause) {
           this.counter.pause();
        }

        this.commonService.getSponserLogo(this.gameStage).subscribe(
            (response:any) => {
                    if(response['status'] == 1) {
                        this.sponser = response.data;
                    }
                },
                error =>{
                    this.toastr.error(error.message);
                }
            )

        this.gameStageService.getGameDetails().subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.gameDetail = data.detail;
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        );

        this.gameStageService.getWorkforce(this.currentUser.team_id).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.contractorDetail = data.detail;
                    this.avgquality = parseFloat(data.avgQuality).toFixed(2);
                    this.avgspeed = parseFloat(data.avgSpeed).toFixed(2);
                    this.avgsafety = parseFloat(data.avgSafty).toFixed(2); 
                    this.totalcost = data.totalCost;
                    localStorage.setItem('totalCost',JSON.parse(this.totalcost));
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        );
    }

    storeTimer(){
        localStorage.removeItem('timer');
        const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
        const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(currentTime); 
        let date2:any = new Date(endtime);

        this.seconds = (date2 - date1)/1000;
    
        localStorage.setItem('timer', this.seconds);

        this.remainingTime =  parseFloat(this.seconds);
    }

    storeTeamStageStatus(){
       
        this.pause = true;
        localStorage.setItem('stagepause',this.pause);
        this.counter.pause();
       
        let currentDateTime: Date = new Date();

        const dateA = this.datepipe.transform(currentDateTime, 'yyyy-MM-dd HH:mm:ss');
        const dateB = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(dateA); 
        let date2:any = new Date(dateB);

        this.seconds = (date2 - date1)/1000;

        this.teamStageStatus = {
            'team_id':this.currentUser.team_id,
            'game_id': this.gameId,
            'stage': this.gameStage,
            'remaining_time':this.seconds,
            'current_route':this.current_route
        }

        this.gameStageService.saveTeamStageStatus(this.teamStageStatus).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                } else if(data['status'] == 0) {
                    this.toastr.error(data['message']);
                } else {
                    this.toastr.error(data['message']);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )

    }

    resumeStage(){

        let resumeDateTime: Date = new Date();

        this.gameStageService.getTeamStageStatus(this.gameStage, this.gameId,this.currentUser.team_id).subscribe(
                            (data:any) => {
                                // status 3 = stage complete and score get ,2 = required parameter missing, 1 = stage pause by team , 0 = start stage
                                if(data.status == 3) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);
                                }else if(data.status == 2) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);  
                                }else if(data.status == 1) {

                                    this.pause = false;
                                    localStorage.setItem('stagepause',this.pause);
                                    
                                    resumeDateTime.setSeconds(resumeDateTime.getSeconds() + JSON.parse(data.detail.remaining_time));
                                    
                                    let endDatetime = this.datepipe.transform(resumeDateTime, 'yyyy-MM-dd HH:mm:ss');
                                    
                                    localStorage.setItem('endtime', endDatetime);

                                    localStorage.setItem('timer', data.detail.remaining_time);
                             
                                    this.remainingTime =  parseFloat(data.detail.remaining_time);
                                    this.counter.resume();
                                }

                            },
                            error =>{
                                this.toastr.error(error.message);
                                this.router.navigate(['stages']);
                            }
                        );
    }

    
}

