import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GameStageService } from '../../__services/game-stage.service';
import { CommonService } from '../../__services/common.service';
import { Router,ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { CountdownComponent } from 'ngx-countdown';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-research-center',
  templateUrl:'./research-center.component.html',
  styleUrls:[],
  providers: [DatePipe]
})

export class ResearchCenter implements OnInit {

	gameStage:string = "1";
	gameId:string = "";
	current_route:string = "";
	seconds:any = "3600";
	stageDeadLine:string = "";
	start_time:string = "";
	pause:any;
	content:any= {};
	gameDetail:any= {};
	teamStageStatus:any = {};
	currentUser:any = {};
	saveAmount:any;

	sponser:any;
	env = environment;

	remainingTime: number = 3600;

	@ViewChild('cd') counter: CountdownComponent;


	public now: Date = new Date();
	public newDate: Date = new Date();

  	constructor (
  		private commonService: CommonService,
  		private gameStageService : GameStageService,
  		private activatedRoute : ActivatedRoute,
  		private router : Router,
		private toastr : ToastrService,
		public datepipe: DatePipe
	) {
	  	var user = localStorage.getItem('currentUser');
	    this.currentUser = JSON.parse(user);
	  }

 	ngOnInit() {

 		this.gameId = localStorage.getItem('gameId');
 		this.current_route = this.activatedRoute.snapshot['_routerState'].url;
 		this.gameStage = this.activatedRoute.snapshot.paramMap.get('stage');
 		this.start_time = this.datepipe.transform(this.newDate, 'yyyy-MM-dd HH:mm:ss');
 		this.stageDeadLine = this.datepipe.transform(this.newDate.setDate(this.newDate.getDate() + 7), 'yyyy-MM-dd HH:mm:ss');

 		this.saveAmount = parseFloat(localStorage.getItem('saving_min'));


 		this.commonService.getSponserLogo(this.gameStage).subscribe(
			(response:any) => {
					if(response['status'] == 1) {
						this.sponser = response.data;
					}
				},
				error =>{
					this.toastr.error(error.message);
				}
			)


 		if (localStorage.getItem("timer") === null) {

 			this.pause = false;
 			localStorage.setItem('stagepause',this.pause);			
 			localStorage.removeItem('timer');
 			localStorage.removeItem('endtime');
            this.now.setHours(this.now.getHours() + 1);
            const endtime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
            
            localStorage.setItem('endtime', endtime);
            localStorage.setItem('timer', this.seconds);
            this.remainingTime =  parseFloat(this.seconds);

            this.storeTeamStageStatus();

        }else{
        	
        	this.pause = JSON.parse(localStorage.getItem('stagepause'));

	        const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
	        const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

	        let date1:any = new Date(currentTime); 
	        let date2:any = new Date(endtime);

	        this.seconds = (date2 - date1)/1000;

	        localStorage.setItem('timer', this.seconds);

	        this.remainingTime =  parseFloat(this.seconds);

	        if (this.pause) {
	           this.counter.pause();
	        }
        }

		if(this.gameStage != "3" && this.gameStage != "4"){
	        this.getStageResearch(this.gameStage,this.gameId);       
		}
		
 	}

 	storeTeamStageStatus(){
 		//if seconds  = 3600 this time called func
 		if(this.seconds != 3600){
 			this.pause = true;
 			localStorage.setItem('stagepause',this.pause);
 			this.counter.pause();
 		}

 		let currentDateTime: Date = new Date();

 		const dateA = this.datepipe.transform(currentDateTime, 'yyyy-MM-dd HH:mm:ss');
	    const dateB = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

	    let date1:any = new Date(dateA); 
	    let date2:any = new Date(dateB);

	    this.seconds = (date2 - date1)/1000;

 		this.teamStageStatus = {
 			'team_id':this.currentUser.team_id,
 			'game_id': this.gameId,
 			'stage': this.gameStage,
 			'start_time': this.start_time,
 			'dead_line':this.stageDeadLine,
 			'remaining_time':this.seconds,
 			'current_route':this.current_route
 		}

 		this.gameStageService.saveTeamStageStatus(this.teamStageStatus).subscribe(
    		(data:any) => {
		        if(data['status'] == 1) {
		        	console.log(data);
		        } else if(data['status'] == 0) {
		            this.toastr.error(data['message']);
		        } else {
		            this.toastr.error(data['message']);
		        }
	    	},
	    	error =>{
				this.toastr.error(error.message);
	    	}
	    )

 	}

 	resumeStage(){

 		let resumeDateTime: Date = new Date();

 		this.gameStageService.getTeamStageStatus(this.gameStage, this.gameId,this.currentUser.team_id).subscribe(
	                        (data:any) => {
	                        	// status 3 = stage complete and score get ,2 = required parameter missing, 1 = stage pause by team , 0 = start stage
	                            if(data.status == 3) {
	                            	this.toastr.success(data.message);
	                            	this.router.navigate(['stages']);
	                            }else if(data.status == 2) {
	                            	this.toastr.success(data.message);
	                            	this.router.navigate(['stages']);  
	                            }else if(data.status == 1) {

	                            	this.pause = false;
	                            	localStorage.setItem('stagepause',this.pause);
	                            	
						            resumeDateTime.setSeconds(resumeDateTime.getSeconds() + JSON.parse(data.detail.remaining_time));
						            console.log(resumeDateTime);
						            let endDatetime = this.datepipe.transform(resumeDateTime, 'yyyy-MM-dd HH:mm:ss');
						            
						            localStorage.setItem('endtime', endDatetime);

						            localStorage.setItem('timer', data.detail.remaining_time);
						     
						            this.remainingTime =  parseFloat(data.detail.remaining_time);
						            this.counter.resume();
                               
	                            }else{
	                             	this.getStageResearch(this.gameStage,this.gameId); 
	                            }

	                        },
	                        error =>{
	                            this.toastr.error(error.message);
	                            this.router.navigate(['stages']);
	                        }
	                    );
 	}

 	getStageResearch(gameStage,gameId) {

		this.gameStageService.gameStageResearchDetail(gameStage,gameId).subscribe(
    		(data:any) => {
		        if(data['status'] == 1) {
		        	
		        	this.content = data.detail

		        } else if(data['status'] == 0) {
		            this.toastr.error(data['message']);
		        } else {
		            this.toastr.error(data['message']);
		        }
	    	},
	    	error =>{
				this.toastr.error(error.message);
	    	}
	    )
    }
}
