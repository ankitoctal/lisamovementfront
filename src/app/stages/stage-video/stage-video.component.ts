import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GameStageService } from '../../__services/game-stage.service';
import { Router,ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-stage-video',
  templateUrl: './stage-video.component.html',
  styleUrls: [],
  providers: [DatePipe]
})

export class StageVideoComponent implements OnInit {

	gameStage:string = "1";
	content:any= {};
	currentUser:any= {};
	gameDetail:any= {};

	public now: Date = new Date();

  	constructor(
  		private gameStageService : GameStageService,
  		private activatedRoute : ActivatedRoute,
    	private router: Router,
  		private toastr : ToastrService,
		  public datepipe: DatePipe
    ) {

     	var user = localStorage.getItem('currentUser');
     	this.currentUser = JSON.parse(user);

    }

  	ngOnInit() {

     	this.gameStage = this.activatedRoute.snapshot.paramMap.get('stage');
     	localStorage.removeItem('timer');
		  localStorage.removeItem('endtime');
      localStorage.removeItem('saving_min');

  		this.startStage();

  	}

    async startStage(){

        await this.gameStageService.getGameDetails().subscribe(
              (data:any) => {
                  if(data['status'] == 1) {
                      this.gameDetail = data.detail;
                      // get complete or pause

                      localStorage.setItem('gameId', this.gameDetail._id);


                      this.gameStageService.getTeamStageStatus(this.gameStage,this.gameDetail._id,this.currentUser.team_id).subscribe(
	                        (data:any) => {
	                        	// status 3 = stage complete and score get ,2 = required parameter missing, 1 = stage pause by team , 0 = start stage
	                            if(data.status == 3) {
	                            	this.toastr.success(data.message);
	                            	this.router.navigate(['stages']);
	                            }else if(data.status == 2) {
	                            	this.toastr.success(data.message);
	                            	this.router.navigate(['stages']);  
	                            }else if(data.status == 1) {

        						            this.now.setSeconds(this.now.getSeconds() + JSON.parse(data.detail.remaining_time));
        						            const endtime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
        						            
        						            localStorage.setItem('endtime', endtime);
        						            localStorage.setItem('timer', data.detail.remaining_time); 

	                            	this.router.navigate([data.detail.current_route]);	                               
	                            }else{
	                             	this.getStageIntro(this.gameStage,this.gameDetail._id); 
	                            }

	                        },
	                        error =>{
	                            this.toastr.error(error.message);
	                            this.router.navigate(['stages']);
	                        }
	                    );
                  }
              },
              error =>{
                  this.toastr.error(error.message);
                  this.router.navigate(['stages']);
              }
          );
    }

  	getStageIntro(gameStage,gameId) {

  		  this.gameStageService.gameStageDetail(gameStage,gameId).subscribe(
      		(data:any) => {
  		        if(data['status'] == 1) {
  		        	this.content = data.detail;
                localStorage.setItem('saving_min', this.content.saving_amount);
  		        } else if(data['status'] == 0) {
  		            this.toastr.error(data['message']);
  		        } else {
  		            this.toastr.error(data['message']);
  		        }
  	    	},
  	    	error =>{
  				this.toastr.error(error.message);
  	    	}
  	    )
    }

}
