import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { UserService } from '../../__services/user.service';
import { CommonService } from '../../__services/common.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-stages-header',
  templateUrl: './header.component.html',
  styleUrls: []
})


export class HeaderComponent implements OnInit {
	public contentSettings : any = {};
	constructor(public userService:UserService,private commonService: CommonService,public router: Router, 
    private toastr: ToastrService) { }

	ngOnInit(): void {
    	this.commonService.gameName('main-page').subscribe((response: any) => {
      		this.contentSettings = response.data; 
    	});
  	}

  	logout() {
    	localStorage.clear();
    	this.router.navigate(['login']);
    	this.toastr.success('User logout');
    }

}
