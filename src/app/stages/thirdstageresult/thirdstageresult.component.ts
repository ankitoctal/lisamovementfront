import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup , Validators, FormBuilder, FormArray } from '@angular/forms';
import { GameStageService } from '../../__services/game-stage.service';
import { GameQuestionService } from '../../__services/game-question.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-thirdstageresult',
  templateUrl: './thirdstageresult.component.html',
  styleUrls: [],
  providers: [DatePipe]
})

export class ThirdstageresultComponent implements OnInit {

	gameId:string = "";
	currentUser:any = {};
    resultDetail:any;
    savedMinute:number = 0;
    gameStage:string = "3";
    result:any = {};

    total:number;

    saveAmount:number = 0;

    public now: Date = new Date();

	constructor(
		private questionService : GameQuestionService,
  		private gameStageService : GameStageService,
        private toastr : ToastrService,
	    private formBuilder: FormBuilder,
	    private router: Router,
        public datepipe: DatePipe) { 
        	var user = localStorage.getItem('currentUser');
		    this.currentUser = JSON.parse(user);
        }

	async ngOnInit() {
		this.gameId = localStorage.getItem('gameId');

		// per min saving amount 
		this.saveAmount = parseFloat(localStorage.getItem('saving_min'));
		var seconds:any;

		
		if (localStorage.getItem("timer") != null) {
			const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
			const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

			let date1:any = new Date(currentTime); 
	        let date2:any = new Date(endtime);

	        seconds = (date2 - date1)/1000;
	        this.savedMinute = Math.floor(seconds / 60);
		}

		await this.getQuestionResults();
		
        localStorage.removeItem('endtime');
        localStorage.removeItem('timer');
	}

	async getQuestionResults(){
		await this.questionService.getQuestionResult(this.currentUser.team_id,this.gameId,this.gameStage).subscribe(
	            (data:any) => {
	            	if(data['status'] == 1) {

	            		this.resultDetail = data.detail;
	            		this.total = data.totalPrice;
	            		console.log(data);

	                    this.result.team_id = this.currentUser.team_id;
	                    this.result.game_id = this.gameId;
	                    this.result.score = (this.saveAmount * this.savedMinute) + this.total;
	                    this.result.savedMin = this.savedMinute;
	                    this.result.stage = "3";
	                    this.result.status = "Complete";
	                    this.result.min_price = this.saveAmount;
	                    this.result.total_spent =  0;

	                    this.saveResult(this.result);
	            	}
	            },
	            error =>{
	                this.toastr.error(error.message);
	            }
	        )
	}

	async saveResult(resultData){
		await this.gameStageService.saveStageResult(resultData).subscribe(
	                        (data:any) => {},
	                        error =>{ this.toastr.error(error.message); }
                    	);
	}

}
