import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup , Validators, FormBuilder  } from '@angular/forms';
import { GameStageService } from '../../__services/game-stage.service';
import { CommonService } from '../../__services/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CountdownComponent } from 'ngx-countdown';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
  selector:'app-stage-choose-workforce',
  templateUrl:'./choose-workforce.component.html',
  styleUrls:[],
  providers: [DatePipe]
})

export class ChooseWorkforce implements OnInit {


    @ViewChild('cd') counter: CountdownComponent;

    gameStage:string = "1";
    gameId:string = "";
    current_route:string = "";

    saveWorkForceForm:any = {};
    teamStageStatus:any = {};
    details:any = {};
    contractor:any = {};
    currentUser:any = {};
    
    concrete:string;
    electrical: string;
    mechanical: string;
    plumbing: string;
    drywall: string;
    curtainwall: string;
    finish: string;

    concreteName: string;
    electricalName: string;
    mechanicalName: string;
    plumbingName: string;
    drywallName: string;
    curtainwallName: string;
    finishName: string;

    pause:any;
    saveAmount:number = 0;
    seconds:any = "3600";
    remainingTime: number = 3600;

    sponser:any;
    env = environment;
    
    public now: Date = new Date();
    public newDate: Date = new Date();

    categoryArray = {
        "1":"Concrete",
        "2":"Electrical",
        "3":"Mechanical",
        "4":"Plumbing",
        "5":"Drywall",
        "6":"Curtain Wall",
        "7":"Finish"
    };
    

    constructor (
        private commonService: CommonService,
        private gameStageService : GameStageService,
        private toastr : ToastrService,
        private formBuilder: FormBuilder,
        private activatedRoute : ActivatedRoute,
        private router: Router,
        public datepipe: DatePipe) {
            var user = localStorage.getItem('currentUser');
            this.currentUser = JSON.parse(user);
        }

    ngOnInit() {

        this.gameId = localStorage.getItem('gameId');
        this.current_route = this.activatedRoute.snapshot['_routerState'].url;

        this.saveAmount = parseFloat(localStorage.getItem('saving_min'));
        
        this.storeTimer();

        this.pause = JSON.parse(localStorage.getItem('stagepause'));

        if (this.pause) {
           this.counter.pause();
        }

        this.commonService.getSponserLogo(this.gameStage).subscribe(
            (response:any) => {
                    if(response['status'] == 1) {
                        this.sponser = response.data;
                    }
                },
                error =>{
                    this.toastr.error(error.message);
                }
            )

        this.saveWorkForceForm = this.formBuilder.group({
            team_id: [],
            concrete: ['',[Validators.required]],
            electrical: ['',[Validators.required]],
            mechanical: ['',[Validators.required]],
            plumbing: ['',[Validators.required]],
            drywall: ['',[Validators.required]],
            curtainwall: ['',[Validators.required]],
            finish: ['',[Validators.required]]
        });

        this.gameStageService.getContractorList().subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.details = data.detail;
                } else if(data['status'] == 0) {
                    this.toastr.error(data.message);
                } else {
                    this.toastr.error(data.message);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )


        this.gameStageService.getWorkforce(this.currentUser.team_id).subscribe(
            (data:any) => {
                if(data['status'] == 1) {

                    if(data.detail.concrete != ""){
                        this.concrete = data.detail.concrete._id;
                        this.concreteName = data.detail.concrete.name;
                    }
                    if(data.detail.electrical != ""){
                        this.electrical = data.detail.electrical._id;
                        this.electricalName = data.detail.electrical.name;
                    }
                    if(data.detail.mechanical != ""){
                        this.mechanical = data.detail.mechanical._id;
                        this.mechanicalName = data.detail.mechanical.name;
                    }
                    if(data.detail.plumbing != ""){
                        this.plumbing = data.detail.plumbing._id;
                        this.plumbingName = data.detail.plumbing.name;
                    }
                    if(data.detail.drywall != ""){
                        this.drywall = data.detail.drywall._id;
                        this.drywallName = data.detail.drywall.name;
                    }
                    if(data.detail.curtainwall != ""){
                        this.curtainwall = data.detail.curtainwall._id;
                        this.curtainwallName = data.detail.curtainwall.name;
                    }
                    if(data.detail.finish != ""){
                        this.finish = data.detail.finish._id;
                        this.finishName = data.detail.finish.name;
                    }
                    
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        );

    }

    storeTimer(){
        localStorage.removeItem('timer');
        const currentTime = this.datepipe.transform(this.now, 'yyyy-MM-dd HH:mm:ss');
        const endtime = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(currentTime); 
        let date2:any = new Date(endtime);

        this.seconds = (date2 - date1)/1000;
    
        localStorage.setItem('timer', this.seconds);

        this.remainingTime =  parseFloat(this.seconds);
    }


    showModal(id) {
        this.gameStageService.getContractor(id).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.contractor = data.detail;
                   
                    $("#contractor").modal('show');
                } else if(data['status'] == 0) {
                    this.toastr.error(data.message);
                } else {
                    this.toastr.error(data.message);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )
        
    }

    selectContractor(category,contractorId,contractorName) {

        if(category == '1'){
            this.concrete = contractorId;
            this.concreteName = contractorName;
        }
        if(category == '2'){
            this.electrical = contractorId;
            this.electricalName = contractorName;
        }
        if(category == '3'){
            this.mechanical = contractorId;
            this.mechanicalName = contractorName;
        }
        if(category == '4'){
            this.plumbing = contractorId;
            this.plumbingName = contractorName;
        }
        if(category == '5'){
            this.drywall = contractorId;
            this.drywallName = contractorName;
        }
        if(category == '6'){
            this.curtainwall = contractorId;
            this.curtainwallName = contractorName;
        }
        if(category == '7'){
            this.finish = contractorId;
            this.finishName = contractorName;
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.saveWorkForceForm.controls; }

    saveWorkForce(){

        if (this.saveWorkForceForm.invalid) {
            
            
            this.toastr.error('All Contractor are required'); 
            
            return;
        }
       
        this.gameStageService.saveWorkforce(this.saveWorkForceForm.value).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                    this.toastr.success(data['message']);
                    this.router.navigate(['stages/review-contractor']);
                   
                } else if(data['status'] == 0) {
                    this.toastr.error(data['message']);
                } else {
                    this.toastr.error(data['message']);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )
    }

    storeTeamStageStatus(){
       
        this.pause = true;
        localStorage.setItem('stagepause',this.pause);
        this.counter.pause();
       
        let currentDateTime: Date = new Date();

        const dateA = this.datepipe.transform(currentDateTime, 'yyyy-MM-dd HH:mm:ss');
        const dateB = this.datepipe.transform(localStorage.getItem('endtime'), 'yyyy-MM-dd HH:mm:ss');

        let date1:any = new Date(dateA); 
        let date2:any = new Date(dateB);

        this.seconds = (date2 - date1)/1000;

        this.teamStageStatus = {
            'team_id':this.currentUser.team_id,
            'game_id': this.gameId,
            'stage': this.gameStage,
            'remaining_time':this.seconds,
            'current_route':this.current_route
        }

        this.gameStageService.saveTeamStageStatus(this.teamStageStatus).subscribe(
            (data:any) => {
                if(data['status'] == 1) {
                } else if(data['status'] == 0) {
                    this.toastr.error(data['message']);
                } else {
                    this.toastr.error(data['message']);
                }
            },
            error =>{
                this.toastr.error(error.message);
            }
        )

    }

    resumeStage(){

        let resumeDateTime: Date = new Date();

        this.gameStageService.getTeamStageStatus(this.gameStage, this.gameId,this.currentUser.team_id).subscribe(
                            (data:any) => {
                                // status 3 = stage complete and score get ,2 = required parameter missing, 1 = stage pause by team , 0 = start stage
                                if(data.status == 3) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);
                                }else if(data.status == 2) {
                                    this.toastr.success(data.message);
                                    this.router.navigate(['stages']);  
                                }else if(data.status == 1) {

                                    this.pause = false;
                                    localStorage.setItem('stagepause',this.pause);
                                    
                                    resumeDateTime.setSeconds(resumeDateTime.getSeconds() + JSON.parse(data.detail.remaining_time));
                                    
                                    let endDatetime = this.datepipe.transform(resumeDateTime, 'yyyy-MM-dd HH:mm:ss');
                                    
                                    localStorage.setItem('endtime', endDatetime);

                                    localStorage.setItem('timer', data.detail.remaining_time);
                             
                                    this.remainingTime =  parseFloat(data.detail.remaining_time);
                                    this.counter.resume();
                                }

                            },
                            error =>{
                                this.toastr.error(error.message);
                                this.router.navigate(['stages']);
                            }
                        );
    }

}
