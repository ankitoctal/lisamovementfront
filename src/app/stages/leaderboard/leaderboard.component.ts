import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../__services/common.service';
import { ToastrService } from 'ngx-toastr';
import { Router,ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: []
})

export class LeaderboardComponent implements OnInit {

	leaderboard:any = [];
	gameStage:any = 1;
	gameId:string = "";

 	constructor(
		private commonService : CommonService,
  		private router : Router,
  		private activatedRoute : ActivatedRoute,
		private toastr : ToastrService,
		) { }

 	ngOnInit() {

 			this.gameStage = this.activatedRoute.snapshot.paramMap.get('stage');
 			this.gameId = localStorage.getItem('gameId');

  			this.commonService.getLeaderboard(this.gameId).subscribe(
	            (data:any) => {
	                if(data['status'] == 1) {
	                    this.leaderboard = data.details;
	                }
	            },
	            error =>{
	                this.toastr.error(error.message);
	                this.router.navigate(['stages']);
	            }
	        );
 	}

 	ConvertToInt(val){
	  return parseInt(val);
	}

	get sortData() {
	    return this.leaderboard.sort((a, b) => {
	      return <any>b.score - <any>a.score;
	    });
	 }

}
