import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroComponent } from './intro/intro.component';
import { ChooseWorkforce } from './choose-workforce/choose-workforce.component';
import { ResearchCenter } from './research-center/research-center.component';
import { ReviewContractorComponent } from './review-contractor/review-contractor.component';
import { ReviewBudgetComponent } from './review-budget/review-budget.component';
import { ResultComponent } from './result/result.component';
import { AuthGuard } from '../auth/auth.guard';
import { TeamComponent } from './team/team.component';
import { StageVideoComponent } from './stage-video/stage-video.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PlacementsComponent} from './placements/placements.component';
import { StageIssueComponent } from './stage-issue/stage-issue.component';
import { SecondstageresultComponent } from './secondstageresult/secondstageresult.component';
import { ThirdstageresultComponent } from './thirdstageresult/thirdstageresult.component';
import { FourstageresultComponent } from './fourstageresult/fourstageresult.component';
import { IssueMessengersComponent } from './issue-messengers/issue-messengers.component';

const routes : Routes = [
  {
    path:'stages',
    children: [
        {
          path: '',
          component: TeamComponent
        },
        {
          path: 'stage-video/:stage',
          component: StageVideoComponent
        },
        {
          path: 'stage-intro/:stage',
          component: IntroComponent
        },
        {
          path: 'placements',
          component: PlacementsComponent
        },
        {
            path: 'research-center/:stage',
            component: ResearchCenter
        },
        {
            path: 'choose-workforce',
            component: ChooseWorkforce
        },
        {
          path: 'review-contractor',
          component: ReviewContractorComponent
        },
        {
          path: 'review-budget',
          component: ReviewBudgetComponent
        },
        {
          path: 'result',
          component: ResultComponent
        },
        {
          path: 'second-stage-result',
          component: SecondstageresultComponent
        },
        {
          path: 'third-stage-result',
          component: ThirdstageresultComponent
        },
        {
          path: 'four-stage-result',
          component: FourstageresultComponent
        },
        {
          path: 'issue-messengers/:stage',
          component: IssueMessengersComponent
        },
        {
          path: 'stage-issue/:stage',
          component: StageIssueComponent
        },
        {
          path: 'leaderboard/:stage',
          component: LeaderboardComponent
        }
    ],
    canActivate:[AuthGuard]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StagesRoutingModule { }
