import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownModule, CountdownGlobalConfig, CountdownConfig } from 'ngx-countdown';
import { StagesRoutingModule } from './stages-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { StagesComponent } from './stages.component';
import { IntroComponent } from './intro/intro.component';
import { ChooseWorkforce } from './choose-workforce/choose-workforce.component';
import { ResearchCenter } from './research-center/research-center.component';
import { ReviewContractorComponent } from './review-contractor/review-contractor.component';
import { ReviewBudgetComponent } from './review-budget/review-budget.component';
import { ResultComponent } from './result/result.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TeamComponent } from './team/team.component';
import { TimerComponent } from './timer/timer.component';
import { StageVideoComponent } from './stage-video/stage-video.component';
import { PlacementsComponent} from './placements/placements.component';
import { PipeModule } from '../pipe/pipes.module';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { StageIssueComponent } from './stage-issue/stage-issue.component';

import { from } from 'rxjs';
import { SecondstageresultComponent } from './secondstageresult/secondstageresult.component';
import { ThirdstageresultComponent } from './thirdstageresult/thirdstageresult.component';
import { FourstageresultComponent } from './fourstageresult/fourstageresult.component';
import { IssueMessengersComponent } from './issue-messengers/issue-messengers.component';


export function countdownConfigFactory(): CountdownConfig {
  return {};
}

@NgModule({

  declarations: [ StagesComponent, IntroComponent, ChooseWorkforce, ResearchCenter, ReviewContractorComponent, ReviewBudgetComponent, ResultComponent, HeaderComponent, SidebarComponent, TeamComponent,StageVideoComponent, TimerComponent, LeaderboardComponent, PlacementsComponent, StageIssueComponent, SecondstageresultComponent, ThirdstageresultComponent, FourstageresultComponent, IssueMessengersComponent],

  imports: [
    CommonModule,
    StagesRoutingModule,
    ReactiveFormsModule,
    PipeModule,
    CountdownModule
  ],
  providers: [{ provide: CountdownGlobalConfig, useFactory: countdownConfigFactory }],
})
export class StagesModule { }
