import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownComponent } from 'ngx-countdown';

@Component({
  selector: 'app-stage-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

	remainingTime: number = 60*10;
	//pause:any;

	@ViewChild('countdown') counter: CountdownComponent;

	constructor() { }

 	ngOnInit(){
 		this.remainingTime =  parseFloat(localStorage.getItem('timer'));
 		 	this.counter.resume();	
 	}

}
