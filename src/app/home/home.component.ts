import { Component, OnInit } from '@angular/core';
import { UserService } from '../__services/user.service';
import { CommonService } from '../__services/common.service';
@Component({
    selector:'app-home',
    templateUrl: './home.component.html',
    styleUrls:[]
})

export class HomeComponent implements OnInit {
  public contentSettings : any = {};
    constructor(
      public userService: UserService,
      private commonService: CommonService
    ) {}

    ngOnInit(): void {
      this.commonService.gameName('main-page').subscribe((response: any) => {
        this.contentSettings = response.data; 
      });
    }
}
