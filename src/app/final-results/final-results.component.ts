import { Component, OnInit } from '@angular/core';
import { CommonService } from '../__services/common.service';
import { GameStageService } from '../__services/game-stage.service';
import { ToastrService } from 'ngx-toastr';
import { Router,ActivatedRoute } from "@angular/router";

@Component({
	selector: 'app-final-results',
	templateUrl: './final-results.component.html',
	styleUrls: []
})

export class FinalResultsComponent implements OnInit {

	leaderboard:any = [];
	gameStage:any = 1;
	gameId:string = "";

	gameDetail:any= {};

	constructor(
		private commonService : CommonService,
		private gameStageService : GameStageService,
  		private router : Router,
  		private activatedRoute : ActivatedRoute,
		private toastr : ToastrService
	) { }

	ngOnInit() {
		this.gameStageService.getGameDetails().subscribe(
              (response:any) => {
                if(response['status'] == 1) {
                   this.gameDetail = response.detail;

		  			this.commonService.getLeaderboard(this.gameDetail._id).subscribe(
			            (data:any) => {
			                if(data['status'] == 1) {
			                    this.leaderboard = data.details;
			                }
			            },
			            error =>{
			                this.toastr.error(error.message);
			                this.router.navigate(['home']);
			            }
			        );
			    }
			},
            error =>{
                this.toastr.error(error.message);
                this.router.navigate(['home']);
            }
        );
	}

	ConvertToInt(val){
	  return parseInt(val);
	}

	get sortData() {
	    return this.leaderboard.sort((a, b) => {
	      return <any>b.score - <any>a.score;
	    });
	 }

}
