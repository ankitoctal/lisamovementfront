import { FormGroup } from '@angular/forms';

export class CustomValidation {

    // custom validator to check that two fields match 

    mustMatch(controlName: string, matchingControlName: string){
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if(matchingControl.errors && !matchingControl.errors.mustMatch){
                return;
            }

            if(control.value !== matchingControl.value){
                matchingControl.setErrors({mustMatch :true});
            }else{
                matchingControl.setErrors(null);
            }
        }

    }

/*
    atLeastOneChecked(minRequired = 1) {
        return (formGroup: FormGroup) => {
            let checked = 0;

            Object.keys(formGroup.controls).forEach(key => {
                const control = formGroup.controls[key];
                if (control.value === true) {
                    checked++;
                }
            });

            if (checked < minRequired) {
                return {
                    requireCheckboxToBeChecked: true,
                };
            }

            return null;
        }
    }*/

}