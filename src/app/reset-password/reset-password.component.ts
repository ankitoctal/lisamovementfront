import { Component, OnInit } from '@angular/core';
import { UserService } from '../__services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup , Validators, FormBuilder } from '@angular/forms';
import { CustomValidation } from '../__helpers/custom-validation.validators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: []
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  submitted = false;
  resetToken: null;
  CurrentState: any;
  IsResetFormValid = true;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private matchConfirmPassword: CustomValidation,
    private toastr:ToastrService,
    private activeRoute: ActivatedRoute,
    private route: Router
    ) {
        this.CurrentState = 'Wait';
        this.activeRoute.params.subscribe(params => {
          this.resetToken = params.token;
          console.log(this.resetToken);
          this.VerifyToken();
        });
    }

  ngOnInit() {

    this.resetPasswordForm = this.formBuilder.group({
      resettoken: [this.resetToken],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    },
    {
      validator: this.matchConfirmPassword.mustMatch('password', 'confirmPassword')
    });

  }

  VerifyToken() {
    this.userService.ValidPasswordToken({ resettoken: this.resetToken }).subscribe(
      data => {
        this.CurrentState = 'Verified';
      },
      err => {
        this.CurrentState = 'NotVerified';
      }
    );
  }

  get f() { return this.resetPasswordForm.controls; }

  resetPassword() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid) {
      return;
    }

    this.userService.resetPassword(this.resetPasswordForm.value).subscribe(
      (data:any) => {
        if(data.status == 1) {
            this.toastr.success(data.message);
            this.route.navigate(['login']);
        } else if(data.status == 2){
          if(data.error.email){
            this.toastr.error( data.error.email.message);
          }
        }else {
            this.toastr.error(data.message);
        }
      },
      error =>{
        this.toastr.error(error);
      }
    )

  }

}
