import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../__services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: []
})
export class ForgetPasswordComponent implements OnInit {

  forgetPasswordForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router:Router,
    private toastr:ToastrService
    ) { }

  ngOnInit() {
    this.forgetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.email]]
    });
  }

  get f() { return this.forgetPasswordForm.controls; }

  forgetPassword() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgetPasswordForm.invalid) {
      return;
    }

    this.userService.forgetPassword(this.forgetPasswordForm.value.email).subscribe(
      (data:any) => {
        console.log(data);
        if(data['status'] == 1) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
            this.toastr.success(data.message);
        } else if(data.status == 2){
          if(data.error.email){
            this.toastr.error( data.error.email.message);
          }
        }else {
            this.toastr.error(data.message);
        }
      },
      error =>{
        this.toastr.error(error.message);
      }
    )

  }

}
