import { Component, OnInit } from '@angular/core';
import { UserService } from '../__services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../__services/common.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: []
})

export class HeaderComponent implements OnInit {
	public mainPageSettings : any = {};
	endDate : Date;
	startDate: Date;
    constructor(
    	public userService: UserService,
  		private activatedRoute : ActivatedRoute,
    	private router: Router,
    	private toastr: ToastrService,
    	private commonService: CommonService
    ) {
      }

    ngOnInit() {
    	
      	this.commonService.gameName('game-settings').subscribe((response: any) => {
        	this.mainPageSettings = response.data; 
      	});
    }

    logout() {
      localStorage.clear();
      this.router.navigate(['login']);
      this.toastr.success('User logout');
    }

}
