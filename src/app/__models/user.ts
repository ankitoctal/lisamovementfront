export class User{
    id: number;
    password: string;
    firstName: string;
    lastName: string;
    teamName: string;
    email: string;
    position: string;
    token: string; 
    team_id: string; 
}