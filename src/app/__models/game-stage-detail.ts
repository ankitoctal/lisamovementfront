export class GameStageDetail{
    id: number;
    gameId: string;
    stage: string;
    video: string;
    researchCenter: string;
    strategyDetail: string;
}